import numpy as np
import matplotlib
import matplotlib.pyplot as plt

def plot_panel_time(save=''):

    data_paths = np.array([ 'data_L128_semi-inf/spec_func/U5_a1_om10_tau80_ntau8001_dt1e-6_f8192', \
                            'data_L128_semi-inf/spec_func/U5_a1_om10_tau80_ntau8001_dt1e-6_f8192', \
                            'data_L128_semi-inf/spec_func/U5_a1_om10_tau80_ntau8001_dt1e-6_f8192', \
                            'data_L128_semi-inf/spec_func/U5_a1_om10_tau80_ntau8001_dt1e-6_f8192' ])
    
    # data_paths = np.array([ 'data_L128_semi-inf/spec_func/U5_a1_om5_tau80_ntau8001_dt1e-6_f8192', \
    #                         'data_L128_semi-inf/spec_func/U5_a1_om5_tau80_ntau8001_dt1e-6_f8192', \
    #                         'data_L128_semi-inf/spec_func/U5_a1_om5_tau80_ntau8001_dt1e-6_f8192', \
    #                         'data_L128_semi-inf/spec_func/U5_a1_om5_tau80_ntau8001_dt1e-6_f8192' ])

    num_kpoints = 128
    timestrs = ['', '0.0', '10.0', '90.0']

    texts = ['a) Eq (bare)', 'b) Non-Eq ($t = 0$)', 'c) Non-Eq ($t = 10$)', 'd) Non-Eq ($t = 90$)']
    
    fig, axs = plt.subplots(1, 4, sharex='col', sharey='row', gridspec_kw={'wspace': 0.11, 'width_ratios': [1,1,1,1.2]}, figsize=(10, 3), dpi=150)

    txtbox_props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)

    # plt.suptitle('top row: $V = 5, \Omega = 4.2t_h$, middle row: $V = 5, \Omega = 10t_h$, bottom row: $V = 10, \Omega = 20t_h$')
    pdata = []
    for i, timestr in enumerate(timestrs):
        data_path = data_paths[i]
        path_le = data_path+"/pump/none/lesser/output_data/t_%s00000/KW.real"%timestr if timestr else data_path+"/no_pump/none/lesser/output_data/t_0.000000/KW.real"
        path_gr = data_path+"/pump/none/greater/output_data/t_%s00000/KW.real"%timestr if timestr else data_path+"/no_pump/none/greater/output_data/t_0.000000/KW.real"
        kw_le = np.flip(np.loadtxt(path_le).T, axis=0)/num_kpoints
        kw_gr = np.flip(np.loadtxt(path_gr).T, axis=0)/num_kpoints
        
        pdata.append(kw_le + kw_gr - 6.77e-7)#- 0.000113)
        # pdata.append(kw_le)

    mis = [np.min(pd) for pd in pdata]
    mas = [np.max(pd) for pd in pdata]
    mi = min(mis); ma = max(mas)
    print(mi, ma)
    vm = abs(mi) if abs(ma) < abs(mi) else abs(ma)
    print("vm =", vm)

    for i, timestr in enumerate(timestrs):
        # freqs = np.linspace(-(7500/150.0)*np.pi, (7500/150.0)*np.pi, pdata[i].shape[0], endpoint=False)
        freqs = np.linspace((-8000/80.0)*np.pi, (8000/80.0)*np.pi, pdata[i].shape[0], endpoint=False)
        
        axs[i].tick_params(axis='y', which='major', pad=20)
        axs[i].set_xlim(0.0, 1.0)
        axs[i].set_xticks([0.0, 0.5, 1.0])
        # axs[i].set_xticklabels([r'$0$', r'$\pi/2$', r'$\pi$'])
        # axs[i].set_xticklabels([r'$-\pi/2$', r'$0$', r'$\pi/2$'], ha='center')
        axs[i].set_xticklabels([r'$-\frac{\pi}{2}$', r'$0$', r'$\frac{\pi}{2}$'], ha='center')
        axs[i].set_yticks([-15, -10, -5, 0, 5, 10, 15])
        axs[i].set_yticklabels([r'$-15$', r'$-10$', r'$~-5$',  r'$\,~~~0$', r'$\,~~~5$', r'$\,~~10$', r'$\,~~15$'], ha='center')
        axs[i].set_ylim(-15.5, 15.5)
       
        # norm = matplotlib.colors.LogNorm(vmin=1e-4, vmax=2e-2)
        norm = matplotlib.colors.SymLogNorm(linthresh=1e-5, linscale=0.5, vmin=-vm, vmax=vm, base=10)
        
        extent=(0.0, 1.0, freqs[0], freqs[-1])
        m = axs[i].imshow(pdata[i], extent=extent, aspect='auto', interpolation='none', cmap='bwr', norm=norm)
        axs[i].text(0.05, 0.82, texts[i], transform=axs[i].transAxes, fontsize=14)#, bbox=txtbox_props)
    
    # kvals = np.linspace(0.0, 1.0, 100)
    # for i in [1]:
    #     axs[i].plot(kvals, -2.5*np.cos(np.pi*kvals), color='k', linestyle='dashed', linewidth=0.4)

    axs[2].set_xlabel('crystal momentum $k a$')
    axs[0].set_ylabel('frequency $\omega/t_h$')
    plt.colorbar(m, ax=axs[-1])
    # axs[0].legend(loc='lower center', bbox_to_anchor=(0.5, 1.0), ncol=3)
    # leg = axs[0].get_legend()
    # for i in range(3):
    #     leg.legendHandles[i].set_color('black')

    # plt.subplots_adjust(left=0.08, right=0.96, top=0.95, bottom=0.16)
    plt.subplots_adjust(left=0.09, right=0.93, top=0.95, bottom=0.24)

    if save:
        plt.savefig(save+'.pdf', format='pdf')
        plt.savefig(save+'.svg', format='svg')
    plt.show()

if __name__ == "__main__":
    # font = {'size': 20}
    # mpl.rc('font', **font)
    # mpl.rc('text', usetex=True)
    # mpl.rc('axes', labelsize=26)
    # matplotlib.rc('axes', labelsize=13)
    # plt.rcParams.update({ 'text.latex.preamble' : r'\usepackage{amsmath}' })
    plt.rcParams.update({'font.size': 18})

    plot_panel_time(save='plots/specfunc_heatmaps')
