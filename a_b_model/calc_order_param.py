#!/usr/bin/python3

import sys
import pathlib

import numpy as np
import numpy.fft as fft
import matplotlib
import matplotlib.pyplot as plt

def A(t, params):
    t_hop, U, ampl, freq = params
    return ampl*np.heaviside(t, 0.5)*np.sin(freq*t)
    # return ampl*t*np.exp(-(t-5.0)**2/2.0)

# time evolution operator
# U(k, t, t - delta_t)
def tstep_Uop(ks, t, dt, params):
    t_hop, U, ampl, freq = params

    dp = lambda l, tt: -2*t_hop*np.cos(l - A(tt, params))

    e = dp(ks, t-0.5*dt)

    Uop = np.zeros((len(ks), 2, 2), dtype=complex)
    Uop += (np.cos(dt*np.sqrt(e*e + 0.25*U*U))[:,None,None] * np.eye(2)[None,:,:])

    fac = (np.sin(dt*np.sqrt(e*e + 0.25*U*U))/np.sqrt(e*e + 0.25*U*U))[:,None,None]
    Uop -= 1j*fac * (e[:,None,None]*np.array([[1,0],[0,-1]])[None,:,:] + 0.5*U*np.array([[0,1],[1,0]])[None,:,:])

    return Uop

def tevol_Uop(ks, t, t0, dt, params):
    t_hop, U_pot, ampl, freq = params

    num_tsteps = int((t - t0)/dt)
    tsteps = np.linspace(t, t0+dt, num_tsteps)

    Uop = np.empty((len(ks), 2, 2))
    Uop = np.eye(2)[None,:,:]

    for tstep in tsteps:
        Uop = tstep_Uop(ks, tstep, dt, params) @ Uop

    return Uop

def ini_occ(ks, params):
    t_hop, U_pot, ampl, freq = params

    dp = lambda l: -2*t_hop*np.cos(l)
    e = dp(ks)

    Uh = 0.5*U_pot
    denom = np.sqrt(2*(e**2 + Uh**2 - e*np.sqrt(e**2 + Uh**2)))
    alpha = Uh/denom
    beta  = (-e + np.sqrt(e**2 + Uh**2))/denom

    r = np.zeros((len(ks), 2, 2))
    r[:,0,0] = beta**2
    r[:,0,1] = -alpha*beta
    r[:,1,0] = -alpha*beta
    r[:,1,1] = alpha**2

    return r

if __name__ == "__main__":

    savedir = sys.argv[1] if len(sys.argv) > 1 else "test/"
    pathlib.Path(savedir).mkdir(parents=True, exist_ok=True)

    # t_hop, U_pot, ampl, freq
    params = [1.0, 5.0, 2.0, 10.0]
    # params = [0.7652, 5.0, 0.0, 10.0]

    dt = 1e-4
    num_kpoints = 128
    
    num_t_hors = 10000
    t_hors = np.linspace(0.0, 100.0, num_t_hors, endpoint=True)
    
    ks = np.linspace(-np.pi/2, np.pi/2, num_kpoints)
    # ks = np.linspace(0.0, np.pi, num_kpoints)

    # horizontal coordinates < c(t) c^+(t') >
    # tau = t' - t
    # gless = < c^+(t') c(t) > = < c^+(t+tau) c(t) >
    # ggrea = < c(t) c^+(t') > = < c(t) c^+(t+tau) >
    gless = np.zeros((num_kpoints, 2, 2, num_t_hors, 1), dtype=complex)
    
    gless[:,:,:,0,0] = ini_occ(ks, params)

    order_params = np.zeros((len(t_hors),), dtype=complex)
    last_t_hor = 0.0
    l_i_t = 0
    for i_t, t_hor in enumerate(t_hors):
        print("tevol from", last_t_hor, "to", t_hor)
        Uop = tevol_Uop(ks, t_hor, last_t_hor, dt, params)
        for a in range(2):
            for b in range(2):
                gless[:,a,b,i_t,0] = np.conj(Uop[:,a,0])*Uop[:,b,0]*gless[:,0,0,l_i_t,0] \
                                   + np.conj(Uop[:,a,1])*Uop[:,b,0]*gless[:,1,0,l_i_t,0] \
                                   + np.conj(Uop[:,a,0])*Uop[:,b,1]*gless[:,0,1,l_i_t,0] \
                                   + np.conj(Uop[:,a,1])*Uop[:,b,1]*gless[:,1,1,l_i_t,0]

        order_params[i_t] = -np.sum(gless[:,0,1,i_t,0] + gless[:,1,0,i_t,0]) \
                / np.sum((gless[:,0,0,i_t,0] + gless[:,1,1,i_t,0]))

        last_t_hor = t_hor
        l_i_t = i_t

    print(order_params)
    pdata = np.zeros((num_t_hors, 2))
    pdata[:,0] = t_hors
    pdata[:,1] = np.real(order_params)
    plt.plot(pdata[:,0], pdata[:,1])
    plt.show()
    np.savetxt(savedir + "/order_param_dt1e-4.txt", pdata)
    
