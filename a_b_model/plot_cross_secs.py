import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator

def plot_panel_time(data_paths, kvals = (), kvals_name = '', save=''):
    
    Ls = [128, 128, 128]
    
    selec = [0,1,2]; ls = len(selec)

    timestrs = [['0.0', '10.0', '90.0'], ['', '', ''], ['', '', '']]
    kval = kvals if kvals else (8, 15, 31)

    texts = ['Non-Eq\n($t = 0$)', 'Non-Eq\n($t = 10$)', 'Non-Eq\n($t = 90$)']
    
    fig, axs = plt.subplots(3, 1, sharex='col', gridspec_kw={'hspace': 0.1, 'height_ratios': [1,1,1]}, figsize=(6, 6), dpi=150)

    pfmt_le = [ { 'color': 'red', 'linestyle': 'solid'}, \
                { 'color': 'black', 'linestyle': 'dashed'}, \
                { 'color': 'grey', 'linestyle': 'dotted'}, \
                { 'color': 'black', 'linestyle': 'solid'} ]

    pfmt_gr = [ { 'color': 'blue', 'linestyle': 'solid'}, \
                { 'color': 'black', 'linestyle': 'dashed'}, \
                { 'color': 'grey', 'linestyle': 'dotted'}, \
                { 'color': 'grey', 'linestyle': 'solid'} ]
    
    plot_labels = [ r'$\tilde{A}^\mathrm{ret}_k(t,\omega)$', \
                    r'Eq eff. $\tilde{A}_k^\mathrm{ret}(\omega)$', \
                    r'Eq bare $\tilde{A}_k^\mathrm{ret}(\omega)$' ]
    
    txtbox_props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)

    # plt.suptitle('Spectral function at $k = %s$' % kvals_name)
    for i_d in range(ls):
        i_s = selec[i_d]
        L = Ls[i_s]
        data_path = data_paths[i_s]
        for i_t, timestr in enumerate(timestrs[i_s]):

            path_le = data_path+"/pump/none/lesser/output_data/t_%s00000/KW.real"%timestr if timestr else data_path+"/no_pump/none/lesser/output_data/t_0.000000/KW.real"
            path_gr = data_path+"/pump/none/greater/output_data/t_%s00000/KW.real"%timestr if timestr else data_path+"/no_pump/none/greater/output_data/t_0.000000/KW.real"
            # kw_le = np.flip(np.loadtxt(path_le).T, axis=0)/L
            # kw_gr = np.flip(np.loadtxt(path_gr).T, axis=0)/L
        
            kw_le = np.loadtxt(path_le)/L
            kw_gr = np.loadtxt(path_gr)/L
        
            kw_sum = kw_le + kw_gr - 6.77e-7# - 0.5*0.00012267# - 2.6e-5
            # kw_sum = kw_le + kw_gr - 7.5e-7# - 0.5*0.000061759

            kw_sum /= np.max(np.abs(kw_sum[kval[i_s],:]))
            # kw_sum /= np.max(np.abs(kw_sum))
            
            kw_sum_pos = np.clip(kw_sum[kval[i_s],:], 0.0, None)
            kw_sum_neg = np.abs(np.clip(kw_sum[kval[i_s],:], None, 0.0))

            print("spectral sum = ", np.sum(kw_sum))
            print(np.max(kw_sum_neg))

            # print(kw_sum[31,:])
            
            # kw_le /= np.max(kw_le[kval[i_s],:])
            # kw_gr /= np.max(kw_gr[kval[i_s],:])

            # freqs = np.linspace(-(2000/50.0)*np.pi, (2000/50.0)*np.pi, kw_le.shape[1], endpoint=False)
            # freqs = np.linspace(-(7500/150.0)*np.pi, (7500/150.0)*np.pi, kw_le.shape[1], endpoint=False)
            freqs = np.linspace(-(8000/80.0)*np.pi, (8000/80.0)*np.pi, kw_le.shape[1], endpoint=False)
            
            axs[i_t].set_xlim(-20.0, 20.0)
            axs[i_t].set_yscale('log')
            # axs[i_t].set_ylim(1e-8, 2e-2)
            # axs[i_t].set_ylim(1e-5, 2e-2)
            axs[i_t].set_ylim(5e-5, 1.0)

            axs[i_t].set_xticks([-20, -15, -10, -5, 0, 5, 10, 15, 20])
            axs[i_t].set_yticks([1e-4, 1e-3, 1e-2, 1e-1, 1])

            if i_t == 0:
                axs[i_t].plot(freqs, kw_sum_pos, **(pfmt_le[i_d]), label=plot_labels[i_d])
            else:
                axs[i_t].plot(freqs, kw_sum_pos, **(pfmt_le[i_d]))

            axs[i_t].plot(freqs, kw_sum_neg, **(pfmt_gr[i_d]))

    axs[-1].set_xlabel('frequency $\omega/t_h$')
    legend = axs[0].legend(loc='lower center', bbox_to_anchor=(0.5, 1.0), ncol=4, frameon=True, fontsize=14, columnspacing=0.5, handletextpad=0.2)
    frame = legend.get_frame()
    frame.set_facecolor('lightgrey')
    frame.set_edgecolor('black')

    # take last i_s for simplicity
    kvals_name = kvals_name[i_s]
    for i_t, timestr in enumerate(timestrs[i_s]):
        axs[i_t].text(0.025, 0.55, kvals_name, transform=axs[i_t].transAxes)
        axs[i_t].text(0.025, 0.8, (['f)', 'g)', 'h)'])[i_t], transform=axs[i_t].transAxes)
        axs[i_t].text(0.8, 0.625, texts[i_t], transform=axs[i_t].transAxes)#, bbox=txtbox_props)
    
    for h in axs[0].get_legend().legend_handles:
        h.set_color('black')

    plt.subplots_adjust(top=0.90, bottom=0.11, left=0.13, right=0.97)

    if save:
        plt.savefig(save+'.pdf', format='pdf')
        plt.savefig(save+'.svg', format='svg')
    plt.show()

if __name__ == "__main__":
    # data_paths = np.array([ 'L128_tau50_ntau2001', \
    #                         'L128_tau50_ntau2001_eff', \
    #                         'L128_tau50_ntau2001' ])
    # 
    # data_paths = np.array([ 'L128_tau20_ntau1001', \
    #                         'L128_tau20_ntau1001_eff', \
    #                         'L128_tau20_ntau1001' ])

    # data_paths = np.array([ 'L128_tau20_ntau1001_om5', \
    #                         'L128_tau20_ntau1001_eff', \
    #                         'L128_tau20_ntau1001_om5' ])

    # data_paths = np.array([ 'L128_tau50_ntau1001_dt1e-6', \
    #                         'L128_tau20_ntau1001_eff', \
    #                         'L128_tau50_ntau1001_dt1e-6' ])
    
    # data_paths = np.array([ 'data_L128/spec_func/U5_a1_om10_tau150_ntau7501_dt1e-3_f8192', \
    #                         'data_L128/spec_func/U5_a1_om10_tau150_ntau7501_dt1e-3_f8192_eff', \
    #                         'data_L128/spec_func/U5_a1_om10_tau150_ntau7501_dt1e-3_f8192' ])

    data_paths = np.array([ 'data_L128_semi-inf/spec_func/U5_a1_om10_tau80_ntau8001_dt1e-6_f8192', \
                            'data_L128_semi-inf/spec_func/U5_a1_om10_tau80_ntau8001_dt1e-6_f8192_eff', \
                            'data_L128_semi-inf/spec_func/U5_a1_om10_tau80_ntau8001_dt1e-6_f8192' ])
    
    # data_paths = np.array([ 'data_L128_semi-inf/spec_func/U5_a1_om5_tau80_ntau8001_dt1e-6_f8192', \
    #                         'data_L128_semi-inf/spec_func/U5_a1_om10_tau80_ntau8001_dt1e-6_f8192_eff', \
    #                         'data_L128_semi-inf/spec_func/U5_a1_om5_tau80_ntau8001_dt1e-6_f8192' ])

    kvals_indices = [(63, 63, 63), (63+32, 63+32, 63+32), (63+64, 63+64, 63+64)]
    kvals_names = ([r'$k a = 0$']*3, [r'$k a \approx \pi/4$',]*3, [r'$k a \approx \pi/2$',]*3)
    # kvals_names = (r'$0$', r'$\pi/4$', r'$\pi/2$')
    kvals_savedirs = ('plots/panel_comp_eff_ret_t_k0', \
                  'plots/panel_comp_eff_ret_t_k25e-2', \
                   'plots/panel_comp_eff_ret_t_k5e-1')

    kvals_indices = [(0, 0, 0), (32, 32, 32), (63, 63, 63)]
    kvals_names = ([r'$k a \approx -\pi/2$',]*3, [r'$k a \approx -\pi/4$',]*3, [r'$k a = 0$']*3)
    # kvals_names = (r'$0$', r'$\pi/4$', r'$\pi/2$')
    kvals_savedirs = ('plots/panel_comp_eff_ret_t_k-5e-1', \
                  'plots/panel_comp_eff_ret_t_k-25e-2', \
                   'plots/panel_comp_eff_ret_t_k0')

    # matplotlib.rc('axes', labelsize=13)
    # plt.rcParams.update({'font.size': 12})
    plt.rcParams.update({'font.size': 18})
    for i in range(3):
        plot_panel_time(data_paths, kvals = kvals_indices[i], kvals_name = kvals_names[i], save = kvals_savedirs[i])
