#!/usr/bin/python3

import sys
import pathlib

import numpy as np
import numpy.fft as fft
import matplotlib
import matplotlib.pyplot as plt

def A(t, params):
    t_hop, U, ampl, freq = params
    return ampl*np.heaviside(t, 0.5)*np.sin(freq*t)
    # return ampl*(t-50.0)*np.exp(-(t-50.0)**2/5.0)

# time evolution operator
# U(k, t, t - delta_t)
def tstep_Uop(ks, t, dt, params):
    t_hop, U, ampl, freq = params

    dp = lambda l, tt: -2*t_hop*np.cos(l - A(tt, params))

    e = dp(ks, t-0.5*dt)

    Uop = np.zeros((len(ks), 2, 2), dtype=complex)
    Uop += (np.cos(dt*np.sqrt(e*e + 0.25*U*U))[:,None,None] * np.eye(2)[None,:,:])

    fac = (np.sin(dt*np.sqrt(e*e + 0.25*U*U))/np.sqrt(e*e + 0.25*U*U))[:,None,None]
    Uop -= 1j*fac * (e[:,None,None]*np.array([[1,0],[0,-1]])[None,:,:] + 0.5*U*np.array([[0,1],[1,0]])[None,:,:])

    return Uop

def tevol_Uop(ks, t, t0, dt, params):
    if (t - t0 + 1e-8) < dt:
        print("# !! Unit matrix, t - t0 =", t - t0, ", dt =", dt)
        return np.eye(2)[None,:,:]
    
    t_hop, U_pot, ampl, freq = params
    num_tsteps = int((t - t0)/dt)
    tsteps = np.linspace(t, t0+dt, num_tsteps)

    Uop = np.empty((len(ks), 2, 2))
    Uop = np.eye(2)[None,:,:]

    for tstep in tsteps:
        Uop = tstep_Uop(ks, tstep, dt, params) @ Uop

    return Uop

def ini_occ(ks, params):
    t_hop, U_pot, ampl, freq = params

    dp = lambda l: -2*t_hop*np.cos(l)
    e = dp(ks)

    Uh = 0.5*U_pot
    denom = np.sqrt(2*(e**2 + Uh**2 - e*np.sqrt(e**2 + Uh**2)))
    alpha = Uh/denom
    beta  = (-e + np.sqrt(e**2 + Uh**2))/denom

    r = np.zeros((len(ks), 2, 2))
    r[:,0,0] = beta**2
    r[:,0,1] = -alpha*beta
    r[:,1,0] = -alpha*beta
    r[:,1,1] = alpha**2

    return r

if __name__ == "__main__":

    savedir = sys.argv[1] if len(sys.argv) > 1 else "test/"
    savepath_le = savedir + "/none/lesser/output_data/"
    savepath_gr = savedir + "/none/greater/output_data/"
    pathlib.Path(savepath_le).mkdir(parents=True, exist_ok=True)
    pathlib.Path(savepath_gr).mkdir(parents=True, exist_ok=True)

    # t_hop, U_pot, ampl, freq
    params = [1.0, 5.0, 1.0, 10.0]
    # params = [0.7652, 5.0, 0.0, 10.0]

    dt = 1e-5
    num_kpoints = 512
    
    num_t_hors = 5
    # t_hors = np.linspace(0.0, 100.0, num_t_hors, endpoint=True)
    # t_hors = np.linspace(0.0, 100.0, num_t_hors, endpoint=True)
    t_hors = [0.0, 10.0, 80.0, 90.0, 100.0]
    
    num_taus = 25001 
    tau_end = 250.0#150.0
    taus = np.linspace(0.0, tau_end, num_taus, endpoint=True)
    dtau = tau_end/(num_taus-1)
    tau_damps = np.exp(-5.0*taus/tau_end)
    # tau_damps = np.exp(-taus*taus/2000.0)

    ks = np.linspace(-np.pi/2, np.pi/2, num_kpoints)
    # ks = np.linspace(0.0, np.pi, num_kpoints)

    # horizontal coordinates < c(t) c^+(t') >
    # tau = t' - t
    # gless = < c^+(t') c(t) > = < c^+(t+tau) c(t) >
    # ggrea = < c(t) c^+(t') > = < c(t) c^+(t+tau) >
    gless = np.zeros((num_kpoints, 2, 2, num_t_hors, num_taus), dtype=complex)
    ggrea = np.zeros((num_kpoints, 2, 2, num_t_hors, num_taus), dtype=complex)
    
    gless[:,:,:,0,0] = ini_occ(ks, params)
    ggrea[:,:,:,0,0] = np.eye(2)[None,:,:] - gless[:,:,:,0,0]

    order_params = np.zeros((len(t_hors),), dtype=complex)
    last_t_hor = 0.0
    l_i_t_hor = 0
    for i_t_hor, t_hor in enumerate(t_hors):
        print("t_hor tevol from", last_t_hor, "to", t_hor, "(step", dt, ")")
        Uop = tevol_Uop(ks, t_hor, last_t_hor, dt, params)
        for a in range(2):
            for b in range(2):
                gless[:,a,b,i_t_hor,0] = np.conj(Uop[:,a,0])*Uop[:,b,0]*gless[:,0,0,l_i_t_hor,0] \
                                       + np.conj(Uop[:,a,1])*Uop[:,b,0]*gless[:,1,0,l_i_t_hor,0] \
                                       + np.conj(Uop[:,a,0])*Uop[:,b,1]*gless[:,0,1,l_i_t_hor,0] \
                                       + np.conj(Uop[:,a,1])*Uop[:,b,1]*gless[:,1,1,l_i_t_hor,0]

        # order_params[i_t] = -np.sum(gless[:,0,1,i_t,0] + gless[:,1,0,i_t,0]) \
        #         / np.sum((gless[:,0,0,i_t,0] + gless[:,1,1,i_t,0]))

        ggrea[:,:,:,i_t_hor,0] = np.eye(2)[None,:,:] - gless[:,:,:,i_t_hor,0]

        last_tau = 0.0
        l_i_tau = 0
        for i_tau, tau in enumerate(taus[1:]):
            print("tau tevol from", t_hor+last_tau, "to", t_hor+tau, "(step", dtau, ")")
            Uop = tevol_Uop(ks, t_hor+tau, t_hor+last_tau, dt, params)
            for a in range(2):
                for b in range(2):
                    gless[:,a,b,i_t_hor,1+i_tau] = np.conj(Uop[:,a,0])*gless[:,0,b,i_t_hor,l_i_tau] \
                                                 + np.conj(Uop[:,a,1])*gless[:,1,b,i_t_hor,l_i_tau]
                    ggrea[:,a,b,i_t_hor,1+i_tau] = np.conj(Uop[:,a,0])*ggrea[:,0,b,i_t_hor,l_i_tau] \
                                                 + np.conj(Uop[:,a,1])*ggrea[:,1,b,i_t_hor,l_i_tau]
            last_tau = tau
            l_i_tau = 1+i_tau

        last_t_hor = t_hor
        l_i_t_hor = i_t_hor

    # print(order_params)
    # plt.plot(t_hors, np.real(order_params))
    # plt.show()

    # Spectral function calculation
    n_om = 32768 #8192
    freqs = 2*np.pi*np.flip(fft.fftshift(fft.fftfreq(n_om, dtau)), axis=-1)
    # freqs = 2*np.pi*np.flip(fft.fftshift(fft.fftfreq(8192, dtau)), axis=-1)
    gless_damp = tau_damps[None,None,None,None,:]*gless
    ggrea_damp = tau_damps[None,None,None,None,:]*ggrea
    # gless_freq = np.flip(fft.fftshift(fft.hfft(gless, n=4095, norm='ortho', axis=-1), axes=-1), axis=-1)
    gless_freq = fft.fftshift(fft.fft(gless_damp, n=n_om, norm='ortho', axis=-1), axes=-1)
    ggrea_freq = fft.fftshift(fft.fft(ggrea_damp, n=n_om, norm='ortho', axis=-1), axes=-1)
    print(gless_freq.shape)

    for i_t, t_hor in enumerate(t_hors):
        gl = np.real(gless_freq[:,0,0,i_t,:] + gless_freq[:,1,1,i_t,:])/128.0
        gr = np.real(ggrea_freq[:,0,0,i_t,:] + ggrea_freq[:,1,1,i_t,:])/128.0

        savepath_le_t = savepath_le + "/t_%.6f/" % t_hor
        savepath_gr_t = savepath_gr + "/t_%.6f/" % t_hor
        pathlib.Path(savepath_le_t).mkdir(parents=True, exist_ok=True)
        pathlib.Path(savepath_gr_t).mkdir(parents=True, exist_ok=True)
        # np.savetxt(savepath_le_t + "KT.real", gless_damp)
        np.savetxt(savepath_le_t + "KW.real", gl)
        # np.savetxt(savepath_gr_t + "KT.real", ggrea_damp)
        np.savetxt(savepath_gr_t + "KW.real", gr)
        
        pdata = gl + gr - 0.5*0.00012267 - 2.6e-5 + 4.3e-5
        c = np.max(np.abs(pdata))
        
        print("sum = ", np.sum(pdata))
        mi = np.min(pdata); ma = np.max(pdata)
        norm = matplotlib.colors.SymLogNorm(linthresh=1e-4, linscale=0.5, vmin=-ma, vmax=ma, base=10)
        extent = (ks[0], ks[-1], freqs[0], freqs[-1])
        plt.ylim(-20.0, 20.0)
        plt.suptitle("t = " +str(t_hor))
        plt.imshow(pdata.T, cmap='bwr', extent=extent, norm=norm, aspect='auto', interpolation='none')
        plt.colorbar()
        plt.show()
        
        # fs = np.linspace(freqs[0], freqs[-1], 4095)
        # plt.plot(freqs, np.sum(gl, axis=0)/c)
        # plt.plot(freqs, np.sum(gr, axis=0)/c)
        # plt.plot(freqs, np.sum(gl + gr, axis=0)/c, label='sum')
        # plt.legend()
        # plt.show()

        # cross sections
        plt.xlim(-10.0, 10.0)
        plt.plot(freqs, pdata[0,:])
        plt.plot(freqs, pdata[128//2,:])
        plt.show()
    
    # t = 1.0
    # Uo = tevol_Uop(ks, t, 0.0, dt, params)
    # for i in range(num_kpoints):
    #     print(Uo[i,:,:] @ np.conj(Uo[i,:,:]).T)
    #     print()
