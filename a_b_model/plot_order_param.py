import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator

if __name__ == "__main__":

    data_path = 'data_L128_semi-inf/order_param/U5_a1_om5.txt'

    savedir = 'plots/order_param.pdf'

    matplotlib.rc('axes', labelsize=13)
    # fig, axs = plt.subplots(3, 1, sharex='col', gridspec_kw={'hspace': 0.1, 'height_ratios': [1,1,1]}, figsize=(6, 6), dpi=150)
    fig = plt.figure(figsize=(12, 4), dpi=150)

    pfmt = [ { 'color': 'red', 'linestyle': 'solid'}, \
             { 'color': 'black', 'linestyle': 'dashed'}, \
             { 'color': 'grey', 'linestyle': 'dotted'}, \
             { 'color': 'black', 'linestyle': 'solid'} ]
      
    plt.xlim(0.0, 100.0)
    # axs[i_t].set_yscale('log')
    plt.ylim(0.725, 0.925)
    plt.xlabel('time $t$ [$1/t_h$]')
    plt.ylabel('order parameter')

    data = np.loadtxt(data_path)
    plt.plot(data[:,0], data[:,1], **(pfmt[0]))

    plt.text(96.0, 0.9, 'e)', fontsize=14)
    # plt.text(0.025, 0.8, 'e)', transform=axs[i_t].transAxes, fontsize=14)

    plt.legend(loc='lower center', bbox_to_anchor=(0.5, 1.0), ncol=4, frameon=False, fontsize=12)

    # for h in axs[0].get_legend().legendHandles:
    #     h.set_color('black')

    plt.tight_layout()
    plt.savefig(savedir, format='pdf')
    plt.show()
