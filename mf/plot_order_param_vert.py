import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator

if __name__ == "__main__":

    data_path = 'data_L64_semi-inf/V8_a1_om30_tau30_ntau3001_dt1e-3_f8192/pump/order_param.txt'
    # data_path = 'data_L64_semi-inf/V12_a1_om30_tau30_ntau3001_dt1e-3_f8192/pump/order_param.txt'

    savedir = 'plots/order_param'

    # matplotlib.rc('axes', labelsize=13)
    plt.rcParams.update({'font.size': 22})
    # fig, axs = plt.subplots(3, 1, sharex='col', gridspec_kw={'hspace': 0.1, 'height_ratios': [1,1,1]}, figsize=(6, 6), dpi=150)
    fig = plt.figure(figsize=(4, 6), dpi=150)

    pfmt = [ { 'color': 'red', 'linestyle': 'solid'}, \
             { 'color': 'black', 'linestyle': 'dashed'}, \
             { 'color': 'grey', 'linestyle': 'dotted'}, \
             { 'color': 'black', 'linestyle': 'solid'} ]
      
    plt.ylim(0.0, 50.0)
    # axs[i_t].set_yscale('log')
    # plt.xlim(0.725, 0.925)
    plt.ylabel('time $t$ [$1/t_h$]')
    plt.xlabel('order parameter')

    data = np.loadtxt(data_path)
    plt.plot(-data[:,1], data[:,0], **(pfmt[0]))

    plt.text(0.93, 46.0, 'e)')
    # plt.text(-0.2, 46.0, 'e)', fontsize=14)
    # plt.text(0.025, 0.8, 'e)', transform=axs[i_t].transAxes, fontsize=14)

    # plt.legend(loc='lower center', bbox_to_anchor=(0.5, 1.0), ncol=4, frameon=False, fontsize=12)

    # for h in axs[0].get_legend().legendHandles:
    #     h.set_color('black')

    plt.subplots_adjust(top=0.98, bottom=0.125, left=0.225, right=0.96)
    plt.savefig(savedir+'.pdf', format='pdf')
    plt.savefig(savedir+'.svg', format='svg')
    plt.show()
