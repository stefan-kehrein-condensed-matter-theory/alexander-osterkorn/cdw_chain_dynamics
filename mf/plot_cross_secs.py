import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator

def plot_panel_time(data_paths, kvals = (), kvals_name = '', save=''):
    
    Ls = [64, 64, 64]
    
    selec = [0,1,2]; ls = len(selec)

    timestrs = [['0e-1', '100e-1', '500e-1'], ['', '', ''], ['', '', '']]
    kval = kvals if kvals else (8, 15, 31)

    texts = ['Non-Eq\n($t = 0$)', 'Non-Eq\n($t = 10$)', 'Non-Eq\n($t = 50$)']
    
    fig, axs = plt.subplots(3, 1, sharex='col', gridspec_kw={'hspace': 0.1, 'height_ratios': [1,1,1]}, figsize=(6, 6), dpi=150)

    pfmt_le = [ { 'color': 'red', 'linestyle': 'solid'}, \
                { 'color': 'black', 'linestyle': 'dashed'}, \
                { 'color': 'grey', 'linestyle': 'dotted'}, \
                { 'color': 'black', 'linestyle': 'solid'} ]

    pfmt_gr = [ { 'color': 'blue', 'linestyle': 'solid'}, \
                { 'color': 'black', 'linestyle': 'dashed'}, \
                { 'color': 'grey', 'linestyle': 'dotted'}, \
                { 'color': 'grey', 'linestyle': 'solid'} ]
    
    plot_labels = [ r'$\tilde{A}^\mathrm{ret}_k(t,\omega)$', \
                    r'Eq eff. $\tilde{A}_k^\mathrm{ret}(\omega)$', \
                    r'Eq bare $\tilde{A}_k^\mathrm{ret}(\omega)$' ]
    
    txtbox_props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)

    # plt.suptitle('Spectral function at $k = %s$' % kvals_name)
    for i_d in range(ls):
        i_s = selec[i_d]
        L = Ls[i_s]
        data_path = data_paths[i_s]
        for i_t, timestr in enumerate(timestrs[i_s]):

            path_le = data_path+"/pump/KW_lesser_t%s.real"%timestr if timestr else data_path+"/no_pump/KW_lesser_t0e-1.real"
            path_gr = data_path+"/pump/KW_greater_t%s.real"%timestr if timestr else data_path+"/no_pump/KW_greater_t0e-1.real"
            kw_le = np.loadtxt(path_le).T/L
            kw_gr = np.loadtxt(path_gr).T/L

            num_freqs = kw_le.shape[1]
            
            offs = 0.00012 if num_freqs == 4096 else 8.25e-5
            kw_sum = kw_le + kw_gr - offs# - 0.00012# - 6.77e-7# - 0.5*0.00012267# - 2.6e-5
            # kw_sum = kw_le + kw_gr - 7.5e-7# - 0.5*0.000061759

            # kw_sum /= np.max(np.abs(kw_sum[kval[i_s],:]))
            kw_sum /= np.max(np.abs(kw_sum))
            
            kw_sum_pos = np.clip(kw_sum[kval[i_s],:], 0.0, None)
            kw_sum_neg = np.abs(np.clip(kw_sum[kval[i_s],:], None, 0.0))

            print("spectral sum = ", np.sum(kw_sum))
            print(np.max(kw_sum_neg))

            # print(kw_sum[31,:])
            
            # kw_le /= np.max(kw_le[kval[i_s],:])
            # kw_gr /= np.max(kw_gr[kval[i_s],:])

            # freqs = np.linspace(-(5000/50.0)*np.pi, (5000/50.0)*np.pi, kw_le.shape[1], endpoint=False)
            freqs = np.linspace(-(3000/30.0)*np.pi, (3000/30.0)*np.pi, num_freqs, endpoint=False)
            
            axs[i_t].set_xlim(-35.0, 35.0)
            axs[i_t].set_yscale('log')
            # axs[i_t].set_ylim(1e-8, 2e-2)
            # axs[i_t].set_ylim(1e-5, 2e-2)
            axs[i_t].set_ylim(5e-4, 1.0)

            axs[i_t].set_xticks([-30, -20, -10, 0, 10, 20, 30])
            axs[i_t].set_yticks([1e-3, 1e-2, 1e-1, 1])

            if i_t == 0:
                axs[i_t].plot(freqs, kw_sum_pos, **(pfmt_le[i_d]), label=plot_labels[i_d])
            else:
                axs[i_t].plot(freqs, kw_sum_pos, **(pfmt_le[i_d]))

            axs[i_t].plot(freqs, kw_sum_neg, **(pfmt_gr[i_d]))

    axs[-1].set_xlabel('frequency $\omega/t_h$')
    # legend = axs[0].legend(loc='lower center', bbox_to_anchor=(0.5, 1.0), ncol=4, frameon=True, fontsize=12)
    legend = axs[0].legend(loc='lower center', bbox_to_anchor=(0.5, 1.0), ncol=4, frameon=True, fontsize=14, columnspacing=0.5, handletextpad=0.2)
    frame = legend.get_frame()
    frame.set_facecolor('lightgrey')
    frame.set_edgecolor('black')

    # take last i_s for simplicity
    kvals_name = kvals_name[i_s]
    for i_t, timestr in enumerate(timestrs[i_s]):
        axs[i_t].text(0.025, 0.8, (['f)', 'g)', 'h)'])[i_t], transform=axs[i_t].transAxes, fontsize=14)
        axs[i_t].text(0.825, 0.725, texts[i_t], transform=axs[i_t].transAxes, fontsize=14)#, bbox=txtbox_props)
        axs[i_t].text(0.025, 0.55, kvals_name, transform=axs[i_t].transAxes, fontsize=18)
    
    for h in axs[0].get_legend().legend_handles:
        h.set_color('black')

    # plt.subplots_adjust(top=0.92, bottom=0.08, left=0.08, right=0.98)
    plt.subplots_adjust(top=0.90, bottom=0.11, left=0.13, right=0.97)

    if save:
        plt.savefig(save+'.pdf', format='pdf')
        plt.savefig(save+'.svg', format='svg')
    plt.show()

if __name__ == "__main__":
    
    # matplotlib.rc('axes', labelsize=13)
    plt.rcParams.update({'font.size': 18})

    # data_paths = np.array([ 'data_L64_semi-inf/V3_a1_om10_tau30_ntau3001_dt1e-3_f8192', \
    #                         'data_L64_semi-inf/V3_a1_om10_tau30_ntau3001_dt1e-3_f8192_eff', \
    #                         'data_L64_semi-inf/V3_a1_om10_tau30_ntau3001_dt1e-3_f8192' ])
    
    # data_paths = np.array([ 'data_L64_semi-inf/V8_a1_om30_tau20_ntau2001_dt1e-3_f4096', \
    #                         'data_L64_semi-inf/V8_a1_om20_tau20_ntau2001_dt1e-3_f4096_eff', \
    #                         'data_L64_semi-inf/V8_a1_om30_tau20_ntau2001_dt1e-3_f4096' ])
    
    data_paths = np.array([ 'data_L64_semi-inf/V8_a1_om30_tau30_ntau3001_dt1e-3_f8192', \
                            'data_L64_semi-inf/V8_a1_om30_tau30_ntau3001_dt1e-3_f8192_eff', \
                            'data_L64_semi-inf/V8_a1_om30_tau30_ntau3001_dt1e-3_f8192' ])
    
    # data_paths = np.array([ 'data_L64_semi-inf/V12_a1_om30_tau30_ntau3001_dt1e-3_f8192', \
    #                         'data_L64_semi-inf/V12_a1_om30_tau30_ntau3001_dt1e-3_f8192_eff', \
    #                         'data_L64_semi-inf/V12_a1_om30_tau30_ntau3001_dt1e-3_f8192' ])

    kvals_indices = [(0, 0, 0), (15, 15, 15), (31, 31, 31)]
    kvals_names = ([r'$k a = 0$']*3, [r'$k a = \pi/2$',]*3, [r'$k a = \pi$',]*3)
    # kvals_names = (r'$0$', r'$\pi/4$', r'$\pi/2$')
    kvals_savedirs = ('plots/panel_comp_eff_ret_t_k0', \
                      'plots/panel_comp_eff_ret_t_k5e-1', \
                      'plots/panel_comp_eff_ret_t_k1')

    for i in range(3):
        plot_panel_time(data_paths, kvals = kvals_indices[i], kvals_name = kvals_names[i], save = kvals_savedirs[i])
