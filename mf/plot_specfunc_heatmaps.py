import numpy as np
import matplotlib
import matplotlib.pyplot as plt

def plot_panel_time(save=''):
    
    # t_hop = 1.0; V_int = 3.0
    # rho_0 = 0.24171115880498384; rho_A = 0.14789833433311309; rho_B = 0.8521016629418927
    # data_paths = np.array([ 'data_L64_semi-inf/V3_a1_om10_tau30_ntau3001_dt1e-3_f8192', \
    #                         'data_L64_semi-inf/V3_a1_om10_tau30_ntau3001_dt1e-3_f8192', \
    #                         'data_L64_semi-inf/V3_a1_om10_tau30_ntau3001_dt1e-3_f8192', \
    #                         'data_L64_semi-inf/V3_a1_om10_tau30_ntau3001_dt1e-3_f8192' ])
    
    # data_paths = np.array([ 'data_L64_semi-inf/V8_a1_om30_tau30_ntau3001_dt1e-3_f8192', \
    #                         'data_L64_semi-inf/V8_a1_om30_tau30_ntau3001_dt1e-3_f8192', \
    #                         'data_L64_semi-inf/V8_a1_om30_tau30_ntau3001_dt1e-3_f8192', \
    #                         'data_L64_semi-inf/V8_a1_om30_tau30_ntau3001_dt1e-3_f8192' ])
    
    t_hop = 1.0; V_int = 8.0
    # eff
    # rho_0 = 0.09198485136610503; rho_A = 0.981821955963687; rho_B = 0.01817804112029845
    rho_0 = 0.11779798872733238; rho_A = 0.97109617929977; rho_B = 0.028903816614111993
    data_paths = np.array([ 'data_L64_semi-inf/V8_a1_om30_tau30_ntau3001_dt1e-3_f8192', \
                            'data_L64_semi-inf/V8_a1_om30_tau30_ntau3001_dt1e-3_f8192', \
                            'data_L64_semi-inf/V8_a1_om30_tau30_ntau3001_dt1e-3_f8192', \
                            'data_L64_semi-inf/V8_a1_om30_tau30_ntau3001_dt1e-3_f8192' ])

    # t_hop = 1.0; V_int = 12.0
    # rho_0 = 0.08111821085370051; rho_A = 0.9865675345950411; rho_B = 0.01343246041070847
    # data_paths = np.array([ 'data_L64_semi-inf/V12_a1_om30_tau30_ntau3001_dt1e-3_f8192', \
    #                         'data_L64_semi-inf/V12_a1_om30_tau30_ntau3001_dt1e-3_f8192', \
    #                         'data_L64_semi-inf/V12_a1_om30_tau30_ntau3001_dt1e-3_f8192', \
    #                         'data_L64_semi-inf/V12_a1_om30_tau30_ntau3001_dt1e-3_f8192' ])

    num_kpoints = 64
    timestrs = ['', '0e-1', '100e-1', '500e-1']
    # timestrs = ['0e-1', '100e-1', '400e-1', '500e-1']

    texts = ['a) Eq (bare)', 'b) Non-Eq ($t = 0$)', 'c) Non-Eq ($t = 10$)', 'd) Non-Eq ($t = 50$)']
    
    fig, axs = plt.subplots(1, 4, sharex='col', sharey='row', gridspec_kw={'wspace': 0.11, 'width_ratios': [1,1,1,1.2]}, figsize=(10, 3), dpi=150)

    txtbox_props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)

    # plt.suptitle('top row: $V = 5, \Omega = 4.2t_h$, middle row: $V = 5, \Omega = 10t_h$, bottom row: $V = 10, \Omega = 20t_h$')
    pdata = []
    for i, timestr in enumerate(timestrs):
        data_path = data_paths[i]
        path_le = data_path+"/pump/KW_lesser_t%s.real"%timestr if timestr else data_path+"/no_pump/KW_lesser_t0e-1.real"
        path_gr = data_path+"/pump/KW_greater_t%s.real"%timestr if timestr else data_path+"/no_pump/KW_greater_t0e-1.real"
        kw_le = np.loadtxt(path_le)/num_kpoints
        kw_gr = np.loadtxt(path_gr)/num_kpoints

        # pdata.append(kw_le + kw_gr - 6.77e-7)#- 0.000113)
        pdata.append(kw_le + kw_gr)

    mis = [np.min(pd) for pd in pdata]
    mas = [np.max(pd) for pd in pdata]
    mi = min(mis); ma = max(mas)
    print(mi, ma)
    vm = abs(mi) if abs(ma) < abs(mi) else abs(ma)
    print("vm =", vm)

    for i, timestr in enumerate(timestrs):
        # freqs = np.linspace((-5000/50.0)*np.pi, (5000/50.0)*np.pi, pdata[i].shape[0], endpoint=False)
        freqs = np.linspace((-3000/30.0)*np.pi, (3000/30.0)*np.pi, pdata[i].shape[0], endpoint=False)
        offs = freqs[1] - freqs[0]
        
        axs[i].tick_params(axis='y', which='major', pad=20)
        axs[i].set_xlim(0.0, 1.0)
        axs[i].set_ylim(-32.0, 32.0)
        axs[i].set_xticks([0.0, 0.5, 1.0])
        # axs[i].set_xticklabels([r'$0$', r'$\pi/2$', r'$\pi$'])
        # axs[i].set_xticklabels([r'$-\pi/2$', r'$0$', r'$\pi/2$'])
        axs[i].set_xticklabels([r'$-\frac{\pi}{2}$', r'$0$', r'$\frac{\pi}{2}$'], ha='center')
        axs[i].set_yticks([-30, -20, -10, 0, 10, 20, 30])
        axs[i].set_yticklabels([r'$-30$', r'$-20$', r'$-10$',  r'$\,~~~0$', r'$\,~~10$', r'$\,~~20$', r'$\,~~30$'], ha='center')
       
        # norm = matplotlib.colors.LogNorm(vmin=1e-4, vmax=2e-2)
        norm = matplotlib.colors.SymLogNorm(linthresh=1e-3, linscale=0.5, vmin=-vm, vmax=vm, base=10)
        
        ks = np.array( list(range(3*(num_kpoints//4), num_kpoints)) + list(range(num_kpoints//4)) )
        # ks = np.array( list(range(num_kpoints//4)) + list(range(3*(num_kpoints//4), num_kpoints)) )
        # ks = np.array( list(range(num_kpoints//2)) )
        extent=(0.0, 1.0, freqs[0], freqs[-1])

        m = axs[i].imshow(pdata[i][:,ks], extent=extent, aspect='auto', interpolation='none', cmap='bwr', norm=norm)

        axs[i].text(0.05, 0.9, texts[i], transform=axs[i].transAxes, fontsize=14)#, bbox=txtbox_props)
    
    # kvals = np.linspace(0.0, 1.0, 100)
    # for i in [1]:
    #     axs[i].plot(kvals, -2.5*np.cos(np.pi*kvals), color='k', linestyle='dashed', linewidth=0.4)

    kps   = np.linspace(0.0, 1.0, 100)
    kvals = np.pi*np.linspace(-0.5, 0.5, 100)
    eps_k = -2.0*0.7652*t_hop*np.cos(kvals)
    # eps_k = -2.0*t_hop*np.cos(kvals)
    chi_k = 2.0*V_int*rho_0*np.cos(kvals)
    E_k = np.sqrt(np.abs(eps_k-chi_k)**2 + V_int**2*(rho_B-rho_A)**2)
    # for i in [3]:
    #     linestyle = 'dotted'
    #     axs[i].plot(kps, -E_k - offs, color='k', linestyle='dotted', linewidth=0.4)
    #     axs[i].plot(kps, +E_k - offs, color='k', linestyle='dotted', linewidth=0.4)
    #     axs[i].plot(kps, (-E_k + E_k[0])/2 - offs, color='k', linestyle='dotted', linewidth=0.4)
    #     axs[i].plot(kps, (+E_k - E_k[0])/2 - offs, color='k', linestyle='dotted', linewidth=0.4)
    #     # axs[i].plot(kvals, -0.7*np.sin(np.pi*kvals), color='k', linestyle='dashed', linewidth=0.4)
    #     # axs[i].plot(kvals, -7.5-0.7*np.sin(np.pi*kvals), color='k', linestyle='dashed', linewidth=0.4)

    axs[2].set_xlabel('crystal momentum $k a$')
    axs[0].set_ylabel('frequency $\omega/t_h$')
    plt.colorbar(m, ax=axs[-1])
    # axs[0].legend(loc='lower center', bbox_to_anchor=(0.5, 1.0), ncol=3)
    # leg = axs[0].get_legend()
    # for i in range(3):
    #     leg.legendHandles[i].set_color('black')

    # plt.subplots_adjust(left=0.08, right=0.96, top=0.95, bottom=0.16)
    plt.subplots_adjust(left=0.09, right=0.93, top=0.95, bottom=0.24)

    if save:
        plt.savefig(save+'.pdf', format='pdf')
        plt.savefig(save+'.svg', format='svg')
    plt.show()

if __name__ == "__main__":
    # font = {'size': 20}
    # mpl.rc('font', **font)
    # mpl.rc('text', usetex=True)
    # mpl.rc('axes', labelsize=26)
    # plt.rcParams.update({ 'text.latex.preamble' : r'\usepackage{amsmath}' })
    # matplotlib.rc('axes', labelsize=13)
    # plt.rcParams.update({ 'text.latex.preamble' : r'\usepackage{amsmath}' })
    plt.rcParams.update({'font.size': 18})

    plot_panel_time(save='plots/specfunc_heatmaps')
